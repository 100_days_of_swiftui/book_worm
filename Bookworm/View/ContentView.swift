//
//  ContentView.swift
//  Bookworm
//
//  Created by Hariharan S on 03/06/24.
//

import SwiftUI
import SwiftData

struct ContentView: View {
    @Query(sort: [
        SortDescriptor(\Book.title),
        SortDescriptor(\Book.author)
    ]) var books: [Book]
    @Environment(\.modelContext) var modelContext

    @State private var showingAddScreen = false
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(self.books) { book in
                    NavigationLink(value: book) {
                        VStack(alignment: .leading) {
                            Text(book.title)
                                .font(.headline)
                            Text(book.author)
                                .foregroundStyle(.secondary)
                        }
                    }
                    .navigationDestination(
                        for: Book.self
                    ) { book in
                        DetailView(book: book)
                    }
                }
                .onDelete(perform: self.deleteBooks)
            }
            .navigationTitle("Bookworm")
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button(
                        "Add Book",
                        systemImage: "plus"
                    ) {
                        self.showingAddScreen.toggle()
                    }
                }
            }
            .sheet(isPresented: self.$showingAddScreen) {
                AddBookView()
            }
        }
    }
    
    func deleteBooks(at offsets: IndexSet) {
        for offset in offsets {
            let book = books[offset]
            self.modelContext.delete(book)
        }
    }
}

#Preview {
    ContentView()
}
