//
//  RatingView.swift
//  Bookworm
//
//  Created by Hariharan S on 05/06/24.
//

import SwiftUI

struct RatingView: View {
    @Binding var rating: Int

    var label = ""
    var maximumRating = 5

    var offImage: Image?
    var onImage = Image(systemName: "star.fill")

    var offColor = Color.gray
    var onColor = Color.yellow
    
    var body: some View {
        HStack {
            if self.label.isEmpty {
                Text(self.label)
            }

            ForEach(
                1..<maximumRating + 1,
                id: \.self
            ) { number in
                Button {
                    self.rating = number
                } label: {
                    self.image(for: number)
                        .foregroundStyle(number > self.rating ? self.offColor : self.onColor)
                }
            }
        }
        .buttonStyle(.plain)
    }
}

// MARK: - Private Methods

private extension RatingView {
    func image(for number: Int) -> Image {
        if number > self.rating {
            self.offImage ?? self.onImage
        } else {
            self.onImage
        }
    }
}

#Preview {
    RatingView(rating: .constant(4))
}
