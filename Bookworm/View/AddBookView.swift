//
//  AddBookView.swift
//  Bookworm
//
//  Created by Hariharan S on 05/06/24.
//

import SwiftUI

struct AddBookView: View {
    @Environment(\.modelContext) var modelContext
    @Environment(\.dismiss) var dismiss
    
    @State private var title = ""
    @State private var rating = 3
    @State private var author = ""
    @State private var review = ""
    @State private var genre = "Fantasy"
    
    @State private var showingAlert = false
    
    let genres = ["Fantasy", "Horror", "Kids", "Mystery", "Poetry", "Romance", "Thriller"]
    
    var body: some View {
        NavigationStack {
            Form {
                Section {
                    TextField(
                        "Name of book",
                        text: self.$title
                    )
                    TextField(
                        "Author's name",
                        text: self.$author
                    )

                    Picker(
                        "Genre",
                        selection: self.$genre
                    ) {
                        ForEach(
                            self.genres,
                            id: \.self
                        ) {
                            Text($0)
                        }
                    }
                }

                Section("Write a review") {
                    TextEditor(text: self.$review)
                    RatingView(rating: self.$rating)
                }
            }
            .navigationTitle("Add Book")
            .alert("Unable to Save", isPresented: self.$showingAlert) {
                Button("OK", role: .cancel) { }
            } message: {
                Text("The Title and Author names are empty")
            }
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button("Save") {
                        guard !self.title.isEmpty,
                              !self.author.isEmpty
                        else {
                            self.showingAlert = true
                            return
                        }
                        
                        let newBook = Book(
                            title: self.title,
                            author: self.author,
                            genre: self.genre,
                            review: self.review,
                            rating: self.rating,
                            date: Date.now
                        )
                        self.modelContext.insert(newBook)
                        self.dismiss()
                    }
                }
            }
        }
    }
}

#Preview {
    AddBookView()
}
