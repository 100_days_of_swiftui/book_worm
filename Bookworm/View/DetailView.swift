//
//  DetailView.swift
//  Bookworm
//
//  Created by Hariharan S on 05/06/24.
//

import SwiftUI
import SwiftData

struct DetailView: View {
    @Environment(\.modelContext) var modelContext
    @Environment(\.dismiss) var dismiss
    @State private var showingDeleteAlert = false
    
    let book: Book
    
    var body: some View {
        ScrollView {
            ZStack(alignment: .bottomTrailing) {
                Image(self.book.genre)
                    .resizable()
                    .scaledToFit()

                Text(self.book.genre.uppercased())
                    .font(.caption)
                    .fontWeight(.black)
                    .padding(8)
                    .foregroundStyle(.white)
                    .background(.black.opacity(0.75))
                    .clipShape(.capsule)
                    .offset(x: -5, y: -5)
            }
            Text(self.book.author)
                .font(.title)
                .foregroundStyle(.secondary)
            
            Text("Added Time: \(self.book.date.formatted(.dateTime))")
                .font(.subheadline)

            Text(self.book.review)
                .padding()

            RatingView(rating: .constant(self.book.rating))
                .font(.largeTitle)
        }
        .navigationTitle(self.book.title)
        .navigationBarTitleDisplayMode(.inline)
        .scrollBounceBehavior(.basedOnSize)
        .alert("Delete book", isPresented: $showingDeleteAlert) {
            Button("Delete", role: .destructive, action: deleteBook)
            Button("Cancel", role: .cancel) { }
        } message: {
            Text("Are you sure?")
        }
        .toolbar {
            Button(
                "Delete this book",
                systemImage: "trash"
            ) {
                self.showingDeleteAlert = true
            }
        }
    }
    
    func deleteBook() {
        self.modelContext.delete(book)
        self.dismiss()
    }
}

#Preview {
    do {
        let config = ModelConfiguration(isStoredInMemoryOnly: true)
        let container = try ModelContainer(for: Book.self, configurations: config)
        let example = Book(
            title: "Test Book",
            author: "Test Author",
            genre: "Horror",
            review: "This was a great book; I really enjoyed it.",
            rating: 4,
            date: Date.now
        )
        return DetailView(book: example)
            .modelContainer(container)
    } catch {
        return Text("Failed to create preview: \(error.localizedDescription)")
    }
}
